# alfa-sdk

This package provides a JavaScript SDK for developing algorithms using [ALFA](https://widgetbrain.com/product/).

## Installation

You can directly install alfa-sdk using [npm](https://www.npmjs.com/). This will install the alfa-sdk package as well as all dependencies.

```sh
$ npm install --save alfa-sdk
```

## Development

To install requirements locally, you can clone this repository, and then run the following command:

```sh
$ npm install
```

## Changelog

- 0.2.0 (2022-06-02)
  - Remove usage of ConfigStore as the credentials and the configurations are stored in AuthStore
- 0.1.21 (2022-04-11)
  - Add new services in endpoints list
- 0.1.20 (2022-03-11)
  - Invoke an algorithm/integration with the run_options defined
- 0.1.19 (2022-02-18)
  - Refactor credentials handling
  - Expose fetchSetting & fetchContext
- 0.1.18 (2021-11-07)
  - Append neo sessionid to auth cookie
- 0.1.17 (2021-10-19)
  - Expose method to just create Endpoint helper based on session
- 0.1.16 (2021-08-16)
  - Allow specifying request options when making a request / invoking an algorithm with a session
- 0.1.15 (2021-08-11)
  - Fetch configuration from config store when no other source of the configuration is available
- 0.1.14 (2021-07-15)
  - Change URL's for public pythia & meta
- 0.1.13 (2021-06-09)
  - Add DataClient
- 0.1.12 (2021-05-28)
  - Improve error handling when error message is not a string
- 0.1.11 (2021-05-21)
  - Prioritize requesting a new token when clientId and clientSecret are specified
- 0.1.10 (2021-05-10)
  - Set non-public alfa arguments on Authentication initialization
- 0.1.9 (2021-04-21)
  - Handle cookie based auth
  - Prioritize credentials in parameters over context
- 0.1.8 (2021-03-22)
  - Use new quinyx domain (web-\*.quinyx.com)
- 0.1.7 (2021-01-04)
  - Handle environment in default configuration
- 0.1.6 (2020-12-16)
  - Added SecretsClient
- 0.1.5 (2020-11-23)
  - Added endpoints for Quinyx ALFA that can be resolved by the EndpointHelper
  - Added alfaID and region to Session and Authentication
- 0.1.4 (2020-10-28)
  - Extend request handling
  - Fix cache client
- 0.1.3 (2020-10-28)
  - Expose clients from root
  - Setup tooling
- 0.1.2 (2020-10-28)
  - Added function argument to IntegrationClient.invoke and definition of function_type
- 0.1.0 - 0.1.1 (2019-09-02)
  - initial version + bugfixes
