const data = require('./data/endpoints.json');
const { formatTemplate } = require('./utils');
const { UnknownServiceError, ServiceEnvironmentError } = require('./exceptions');

//

exports.EndpointHelper = class EndpointHelper {
  constructor(alfaEnv = 'production', alfaID = 'public', region = 'eu-central-1') {
    this.setProperties(alfaEnv, alfaID, region);
  }

  // Moved to a separate function to enable testing of the session with multiple configurations
  setProperties(alfaEnv, alfaID, region) {
    this.services = data[alfaID].services;
    this.hostname = data[alfaID].hostname;
    this.domain = data[alfaID].domain;
    this.alfaEnv = EndpointHelper.resolveEnvironment(alfaEnv);
    this.alfaID = alfaID;
    this.region = region;
  }

  resolve(service, path = '', { environment } = {}) {
    const serviceList = this.services[service];
    if (serviceList === undefined) {
      throw new UnknownServiceError({
        serviceName: service,
        knownServiceNames: Object.keys(this.services).join(', '),
      });
    }

    environment = EndpointHelper.resolveEnvironment(environment || this.alfaEnv);
    let host = serviceList[environment];
    if (host === undefined) {
      throw new ServiceEnvironmentError({ serviceName: service, environment });
    }

    let protocol = 'https';
    if (Array.isArray(host)) {
      [protocol, host] = host;
    }

    const baseUrl = formatTemplate(this.hostname, {
      protocol,
      host,
      domain: this.domain[this.alfaEnv],
      region: this.region,
    });
    return `${baseUrl}${path}`;
  }

  static resolveEnvironment(env) {
    if (env.toLowerCase().startsWith('prod')) return 'prod';
    if (env.toLowerCase().startsWith('test')) return 'test';
    if (env.toLowerCase().startsWith('dev')) return 'dev';
    return env;
  }
};
