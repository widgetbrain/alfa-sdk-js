const _ = require('lodash');
const axios = require('axios');

const { DEFAULT_SETTINGS, SETTING_KEYS, NEO_SESSION_ID_KEY } = require('./data/config.json');
const { Authentication } = require('./auth');
const { EndpointHelper } = require('./helpers');
const { appendCookie } = require('./utils');
const {
  RequestError,
  ResourceNotFoundError,
  AuthenticationError,
  AuthorizationError,
  ValidationError,
} = require('./exceptions');
const { AuthStore } = require('./stores');

//

class Session {
  constructor(
    auth,
    context = {},
    alfaEnv = DEFAULT_SETTINGS.alfaEnv,
    alfaID = DEFAULT_SETTINGS.alfaID,
    region = DEFAULT_SETTINGS.platformRegion,
  ) {
    this.auth = auth;
    this.context = context;
    this.endpoint = new EndpointHelper(alfaEnv, alfaID, region);
    this.defaultOptions = {};

    const options = this.auth.authenticateRequest({});
    options.validateStatus = () => true;
    this.httpSession = axios.create(options);
  }

  static async initialize(credentials, options = {}) {
    const context = fetchContext(options.context);
    const alfaEnv = fetchSetting('alfaEnv', options, context);
    const alfaID = fetchSetting('alfaID', options, context);
    const region = fetchSetting('platformRegion', options, context);
    credentials = mergeCredentials(credentials, context);

    const auth = await Authentication.initialize(credentials, alfaEnv, alfaID, region);
    return new Session(auth, context, alfaEnv, alfaID, region);
  }

  static initializeEndpointHelper(options = {}) {
    const context = fetchContext(options.context);
    const alfaEnv = fetchSetting('alfaEnv', options, context);
    const alfaID = fetchSetting('alfaID', options, context);
    const region = fetchSetting('platformRegion', options, context);

    return new EndpointHelper(alfaEnv, alfaID, region);
  }

  //

  async setDefault(options) {
    this.defaultOptions = options;
  }

  async request({ service, environment, path, prefix, ...options } = {}) {
    service = service || this.defaultOptions.service;
    prefix = prefix || this.defaultOptions.prefix || '';
    environment = environment || this.defaultOptions.environment;

    path = `${prefix}${path}`;
    const url = this.endpoint.resolve(service, path, { environment });

    options = { ...this.defaultOptions, ...options, url };

    let res;
    try {
      res = await this.httpSession.request(options);
    } catch (e) {
      throw Session.parseError(e);
    }

    if (options.parse !== false) return Session.parseResponse(res);
    return res;
  }

  //

  async invoke(functionId, environment, problem, options = {}) {
    if (!_.isPlainObject(problem)) {
      try {
        problem = JSON.parse(problem);
      } catch (err) {
        throw new TypeError('Problem must be a valid JSON string or a dict.');
      }
    }

    const { functionType } = options;

    if (functionType === 'integration') {
      return this.invokeIntegration(functionId, environment, problem, options);
    }

    return this.invokeAlgorithm(functionId, environment, problem, options);
  }

  async invokeIntegration(integrationId, environment, problem, options = {}) {
    const { functionName, returnReference, returnHoldingResponse, requestOptions } = options;

    const params = {
      returnReference,
      returnHoldingResponse,
    };

    return this.request({
      ...requestOptions,
      method: 'post',
      service: 'ais',
      path: `/api/Integrations/${integrationId}/environments/${environment}/functions/${functionName}/invoke`,
      data: problem,
      params,
    });
  }

  async invokeAlgorithm(algorithmId, environment, problem, options = {}) {
    const {
      returnHoldingResponse,
      includeDetails,
      canBuffer,
      returnReference,
      usePreProcessingIntegration,
      usePostProcessingIntegration,
      useExternalLogging,
      ...requestOptions
    } = options;

    const body = {
      algorithmId,
      environment,
      problem,
      returnHoldingResponse,
      includeDetails,
      canBuffer,
      returnReference,
      dryRunOptions: {
        usePreProcessingIntegration,
        usePostProcessingIntegration,
        useExternalLogging,
      },
    };

    return this.request({
      ...requestOptions,
      method: 'post',
      service: 'baas',
      path: '/api/Algorithms/submitRequest',
      data: body,
    });
  }
  //

  static parseResponse(res) {
    const url = res.config.url;
    let data = res.data;
    try {
      data = JSON.parse(data);
    } catch (e) {
      //
    }

    if (_.isObject(data) && _.has(data, 'error') && _.isObject(data.error)) {
      const error = data.error;

      if (error.code) {
        if (['RESOURCE_NOT_FOUND'].includes(error.code)) {
          throw new ResourceNotFoundError({
            url,
            error: JSON.stringify(error),
          });
        }
        if (['MISSING_TOKEN', 'INVALID_TOKEN'].includes(error.code)) {
          throw new AuthenticationError({ error: JSON.stringify(error) });
        }
        if (['UNAUTHORIZED', 'ACCESS_DENIED'].includes(error.code)) {
          throw new AuthorizationError({ url, error: JSON.stringify(error) });
        }
        if (['VALIDATION_FAILED'].includes(error.code)) {
          throw new ValidationError({ error: JSON.stringify(error) });
        }
      }

      // backward compatibility
      if (error.message && typeof error.message === 'string') {
        if (error.message.startsWith('No token provided')) {
          throw new AuthenticationError({ error: JSON.stringify(error) });
        }
        if (error.message.startsWith('Error checking token')) {
          throw new AuthenticationError({ error: JSON.stringify(error) });
        }
      }

      // backward compatibility
      if (error.name) {
        if (error.name === 'ModelNotFoundError') {
          throw new ResourceNotFoundError({ url });
        }
        if (error.name === 'AuthorizationError') {
          throw new AuthorizationError({ url, error: JSON.stringify(error) });
        }
      }

      throw new RequestError({
        url,
        status: res.status,
        error: JSON.stringify(error),
      });
    }

    if (res.status === 403) {
      throw new AuthorizationError({ url, error: JSON.stringify(data) });
    }

    if (res.status < 200 || res.status >= 300) {
      throw new RequestError({
        url,
        status: res.status,
        error: JSON.stringify(data),
      });
    }

    return data;
  }

  static parseError(error) {
    if (!error.isAxiosError) return error;

    const errorData = error.toJSON();
    const codeStatusMap = { ECONNABORTED: 408 };

    const parsedError = new RequestError({
      url: errorData.config.url,
      status: codeStatusMap[errorData.code] || 500,
      error: `${errorData.code}: ${errorData.message}`,
    });
    parsedError.status = codeStatusMap[errorData.code] || 500;

    return parsedError;
  }
}

//

function fetchContext(context) {
  if (_.isPlainObject(context)) return context;

  let envContext = process.env.ALFA_CONTEXT;
  try {
    envContext = JSON.parse(envContext);
  } catch (err) {
    //
  }

  if (_.isPlainObject(envContext)) return envContext;

  return {};
}

function fetchSetting(name, configuration = {}, context = {}) {
  if (configuration[SETTING_KEYS.configuration[name]]) {
    return configuration[SETTING_KEYS.configuration[name]];
  }

  if (process.env[SETTING_KEYS.envvar[name]]) {
    return process.env[SETTING_KEYS.envvar[name]];
  }

  if (context[SETTING_KEYS.context[name]]) {
    return context[SETTING_KEYS.context[name]];
  }

  if (AuthStore.getValue(SETTING_KEYS.authStore[name], { section: AuthStore.CONFIG_SECTION })) {
    return AuthStore.getValue(SETTING_KEYS.authStore[name], {
      section: AuthStore.CONFIG_SECTION,
    });
  }

  if (DEFAULT_SETTINGS[name] === undefined) {
    throw new ValidationError({ error: `${name} setting does not exist.` });
  }

  return DEFAULT_SETTINGS[name];
}

function mergeCredentials(credentials = {}, context = {}) {
  if (!_.isPlainObject(credentials)) credentials = {};
  if (!_.isPlainObject(context)) return credentials;

  if (!credentials.token) {
    if (context.accessToken) credentials.token = context.accessToken;
    if (context.auth0Token) credentials.token = context.auth0Token;
    if (context.token) credentials.token = context.token;
  }

  if (!credentials.cookie) {
    if (context.cookie) credentials.cookie = context.cookie;
  }

  if (context.neoSessionId) {
    const cookie = appendCookie(credentials.cookie, NEO_SESSION_ID_KEY, context.neoSessionId);
    credentials.cookie = cookie;
  }

  return credentials;
}

//

module.exports = { Session, fetchContext, fetchSetting };
