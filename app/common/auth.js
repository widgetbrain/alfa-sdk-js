const _ = require('lodash');
const axios = require('axios');

const { EndpointHelper } = require('./helpers');
const { ALFA_APP_ID } = require('./data/config.json');
const { CredentialsError, TokenNotFoundError, AuthenticationError } = require('./exceptions');
const { AuthStore } = require('./stores');

//

class Authentication {
  constructor(data) {
    this.user = data.user;
    this.token = data.token;
    this.cookie = data.cookie;
  }

  static async initialize(credentials, alfaEnv, alfaID, platformRegion) {
    const endpoint = new EndpointHelper(alfaEnv, alfaID, platformRegion);
    const urlCore = endpoint.resolve('core');

    const data = await authenticate(urlCore, credentials);
    return new Authentication(data);
  }

  //

  getToken() {
    if (!this.token) {
      throw new TokenNotFoundError();
    }

    return this.token;
  }

  authenticateRequest(options) {
    return authenticateRequest(options, this.token, this.cookie);
  }

  //

  static fetchTokens(credentials = {}) {
    const store = AuthStore.getValues();
    const cache = AuthStore.getValues(AuthStore.CACHE_SECTION);
    let token = credentials.token;
    if (!token) {
      if (process.env.ALFA_ACCESS_TOKEN) token = process.env.ALFA_ACCESS_TOKEN;
      if (process.env.ALFA_AUTH0_TOKEN) token = process.env.ALFA_AUTH0_TOKEN;
      if (process.env.ALFA_TOKEN) token = process.env.ALFA_TOKEN;
      if (store && 'token' in store) token = store.token;
      if (cache && 'token' in cache) token = cache.token;
    }

    return token;
  }

  static fetchCookies(credentials = {}) {
    const cookie = credentials.cookie;
    return cookie;
  }

  static fetchCredentials(credentials = {}) {
    if (!credentials.clientId) {
      const store = AuthStore.getValues();

      if (process.env.ALFA_CLIENT_ID) credentials.clientId = process.env.ALFA_CLIENT_ID;
      else if (store && 'client_id' in store) credentials.clientId = store.client_id;

      if (process.env.ALFA_CLIENT_SECRET) credentials.clientSecret = process.env.ALFA_CLIENT_SECRET;
      else if (store && 'client_secret' in store) credentials.clientSecret = store.client_secret;
    }

    if (!credentials.clientId || !credentials.clientSecret) {
      throw new CredentialsError();
    }

    return {
      clientId: credentials.clientId,
      clientSecret: credentials.clientSecret,
    };
  }

  //

  static async requestOauthToken(urlCore, clientId, clientSecret) {
    const url = `${urlCore}/api/ApiKeyValidators/requestToken`;
    let res;

    try {
      const data = {
        clientId,
        clientSecret,
        audience: urlCore,
      };

      res = await axios.post(url, data);
      res = res.data;

      if (res.error) throw res.error;
    } catch (err) {
      throw new AuthenticationError({ error: err });
    }

    const token = `${res.token_type} ${res.access_token}`;
    return token;
  }

  static async validateToken(urlCore, token, cookie = undefined) {
    if (!token && !cookie) {
      throw new AuthenticationError({ error: 'No tokens were supplied' });
    }

    let res;
    try {
      const url = `${urlCore}/api/ApiKeyValidators/validateTokenForApp`;
      let options = { url, method: 'GET', params: { appId: ALFA_APP_ID } };
      options = authenticateRequest(options, token, cookie);

      res = await axios(options);
      res = res.data;

      if (res.error) throw res.error;
    } catch (err) {
      throw new AuthenticationError({ error: err });
    }

    return res;
  }
}

module.exports = { Authentication };

//

async function authenticate(urlCore, credentials = {}) {
  const cookie = Authentication.fetchCookies(credentials);

  // Authenticate using directly set credentials
  if ('clientId' in credentials && 'clientSecret' in credentials) {
    const { clientId, clientSecret } = credentials;
    const token = await Authentication.requestOauthToken(urlCore, clientId, clientSecret);

    return {
      user: await Authentication.validateToken(urlCore, token),
      token,
      cookie,
    }
  }

  try {
    // Authenticate using existing token / cookie
    const token = Authentication.fetchTokens(credentials);
    if (!token && !cookie) {
      throw new TokenNotFoundError();
    }

    return {
      user: await Authentication.validateToken(urlCore, token, cookie),
      cookie,
      token,
    };
  } catch (err) {

    // Authenticate using globally set credentials
    const { clientId, clientSecret } = Authentication.fetchCredentials(credentials);
    const token = await Authentication.requestOauthToken(urlCore, clientId, clientSecret);

    return {
      user: await Authentication.validateToken(urlCore, token),
      token,
    };
  }
}

function authenticateRequest(options, token, cookie = undefined) {
  if (!token && !cookie) throw new TokenNotFoundError();
  if (!options.params) options.params = {};
  if (!options.headers) options.headers = {};

  if (token) {
    options.headers['wb-authorization'] = token;
  }
  if (cookie) {
    options.headers.cookie = cookie;
  }

  return options;
}
