exports.formatTemplate = function formatTemplate(template, params) {
  let message = template;
  let key = [];

  while (key) {
    const regex = /{{([^}]*)}}/;
    key = regex.exec(message);
    if (!key) break;

    message = message.replace(key[0], String(params[key[1]]));
  }

  return message;
};

exports.appendCookie = function appendCookie(cookie, key, value) {
  if (!value || !key) {
    return cookie;
  }
  if (!cookie) {
    cookie = '';
  }

  const cookies = cookie
    .split(';')
    .map((string) => string.trim())
    .filter((string) => string !== '')
    .filter((string) => !string.startsWith(`${key}=`));

  cookies.push(`${key}=${value}`);
  return cookies.join('; ');
};
