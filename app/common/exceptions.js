/* eslint-disable max-classes-per-file */

const { formatTemplate } = require('./utils');

class AlfaError extends Error {
  constructor(params = {}, code, status) {
    const template = params.template || 'An unspecified error occurred';
    const message = formatTemplate(template, params);

    super(message);
    this.name = this.constructor.name;
    this.code = code || 'UNKNOWN_ERROR';
    this.status = status || 500;
  }
}

//

class PermissionError extends AlfaError {
  constructor(params = {}) {
    const template = 'Access denied.';

    params.template = params.template || template;
    super(params, 'ACCESS_DENIED', 403);
  }
}

class ResourceError extends AlfaError {
  constructor(params = {}) {
    const template = 'Requested resource was not found.';

    params.template = params.template || template;
    super(params, 'RESOURCE_NOT_FOUND', 400);
  }
}

class ValidationError extends AlfaError {
  constructor(params = {}) {
    const template = 'Action did not pass validation: {{error}}';

    params.template = params.template || template;
    super(params, 'VALIDATION_FAILED', 400);
  }
}

class AuthenticationError extends AlfaError {
  constructor(params = {}) {
    const template = 'Error encountered during authentication: {{error}}';

    params.template = params.template || template;
    super(params, 'INVALID_TOKEN', 401);
  }
}

class AuthorizationError extends AlfaError {
  constructor(params = {}) {
    const template = 'Not authorized for request to {{url}}: {{error}}';

    params.template = params.template || template;
    super(params, 'UNAUTHORIZED', 401);
  }
}

class RequestError extends AlfaError {
  constructor(params = {}) {
    const template = 'Error encountered during request to {{url}} ({{status}}): {{error}}';

    params.template = params.template || template;
    super(params, 'REQUEST_FAILED');
  }
}

//

class ResourceNotFoundError extends ResourceError {
  constructor(params = {}) {
    const template =
      'Error encountered during resource initialization: Resource not found in ({{url}}): {{error}}';

    params.template = params.template || template;
    super(params);
  }
}

class ResourceDeletionError extends ValidationError {
  constructor(params = {}) {
    const template = 'Cannot delete {{resource}}: {{error}}';

    params.template = params.template || template;
    super(params);
  }
}

class CredentialsError extends ValidationError {
  constructor(params = {}) {
    const template = 'Authentication credentials not found / invalid';

    params.template = params.template || template;
    super(params);
  }
}

class TokenNotFoundError extends CredentialsError {
  constructor(params = {}) {
    const template = 'Authentication tokens not found';

    params.template = params.template || template;
    super(params);
  }
}

class UnknownServiceError extends ValidationError {
  constructor(params = {}) {
    const template =
      "Unknown service: '{{serviceName}}'. Valid service names are: {{knownServiceNames}}";

    params.template = params.template || template;
    super(params);
  }
}

class ServiceEnvironmentError extends ValidationError {
  constructor(params = {}) {
    const template = "Environment '{{environment}}' does not exist for service '{{serviceName}}'";

    params.template = params.template || template;
    super(params);
  }
}

class SemanticVersionError extends ValidationError {
  constructor(params = {}) {
    const template = "Version '{{version}}' does not comply with Semantic Versioning.";

    params.template = params.template || template;
    super(params);
  }
}

class AlfaConfigError extends ValidationError {
  constructor(params = {}) {
    const template = '{{message}} ({{error}})';

    params.template = params.template || template;
    super(params);
  }
}

//

module.exports = {
  AlfaError,
  ResourceError,
  ValidationError,
  PermissionError,
  AuthenticationError,
  AuthorizationError,
  RequestError,
  ResourceNotFoundError,
  ResourceDeletionError,
  CredentialsError,
  TokenNotFoundError,
  UnknownServiceError,
  ServiceEnvironmentError,
  SemanticVersionError,
  AlfaConfigError,
};
