const { Session } = require('./session');

class BaseClient {
  constructor(session, { teamId } = {}) {
    this.session = session;
    this.user = session.auth.user;

    if (!teamId) teamId = this.user.teamId;
    this.teamId = teamId;
  }

  static async initialize(options = {}) {
    let session = options.session;
    if (!session) {
      session = await Session.initialize(options.credentials, options);
    }

    return new BaseClient(session);
  }
}

module.exports = { BaseClient };
