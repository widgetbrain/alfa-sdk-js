/* eslint-disable max-classes-per-file */

const os = require('os');
const fs = require('fs');
const path = require('path');
const ini = require('ini');
const tempy = require('tempy');
const { AlfaError } = require('./exceptions');

const DEFAULT_SECTION = 'default';
const CONFIG_SECTION = 'config';
const CACHE_SECTION = 'cache';

class BaseStore {
  constructor(FILE_NAME) {
    this.FILE_NAME = FILE_NAME || null;
    this.DEFAULT_SECTION = DEFAULT_SECTION;
    this.CONFIG_SECTION = CONFIG_SECTION;
    this.CACHE_SECTION = CACHE_SECTION;
    this.CACHE = '';
  }

  static getConfigDir() {
    const paths = [os.homedir(), tempy.root];
    for (const dirPath of paths) {
      let writable = true;
      try {
        fs.accessSync(dirPath, fs.constants.W_OK);
      } catch (err) {
        writable = false;
      }

      if (writable) return dirPath;
    }

    return null;
  }

  getFilePath(create = true) {
    if (this.FILE_NAME === null) {
      throw new AlfaError({ template: 'You must implement BaseStore and override FILE_NAME' });
    }

    const homePath = BaseStore.getConfigDir();
    if (homePath === null) return null;

    const basePath = path.resolve(homePath, '.config');
    const dirPath = path.resolve(basePath, 'alfa');

    if (create) {
      if (!fs.existsSync(basePath)) fs.mkdirSync(basePath);
      if (!fs.existsSync(dirPath)) fs.mkdirSync(dirPath);
    }

    return path.resolve(dirPath, this.FILE_NAME);
  }

  getStore() {
    const filePath = this.getFilePath();
    if (filePath === null) {
      return ini.parse(this.CACHE);
    }

    if (!fs.existsSync(filePath)) {
      fs.writeFileSync(filePath, '');
    }

    return ini.parse(fs.readFileSync(filePath, 'utf-8'));
  }

  getValues(section = DEFAULT_SECTION) {
    const store = this.getStore();
    if (section in store) {
      return store[section];
    }

    return null;
  }

  purge() {
    const filePath = this.getFilePath();
    if (filePath === null) {
      this.CACHE = '';
    }
    if (fs.existsSync(filePath)) {
      fs.unlinkSync(filePath);
    }
  }

  //

  getValue(key, { section, default: defaultValue } = { section: DEFAULT_SECTION, default: null }) {
    const store = this.getStore();
    if (!(section in store) || !(key in store[section])) {
      return defaultValue;
    }

    return store[section][key];
  }

  setValue(key, value, { section } = { section: DEFAULT_SECTION }) {
    const keyvalues = {};
    keyvalues[key] = value;

    this.setValues(keyvalues, { section });
  }

  setValues(keyvalues = {}, { section } = { section: DEFAULT_SECTION }) {
    const filePath = this.getFilePath();
    const store = this.getStore();

    if (!(section in store)) store[section] = {};
    Object.entries(keyvalues).forEach(([key, value]) => {
      store[section][key] = value;
    });

    if (filePath === null) {
      this.CACHE = ini.stringify(store);
    } else {
      fs.writeFileSync(filePath, ini.stringify(store));
    }
  }
}

module.exports = {
  BaseStore,
  AuthStore: new BaseStore('credentials'),
};
