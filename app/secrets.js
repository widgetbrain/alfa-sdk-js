const { BaseClient } = require('./common/base');

class SecretsClient extends BaseClient {
  constructor(session, { teamId } = {}) {
    super(session, { teamId });

    this.session.setDefault({
      service: 'core',
      prefix: '/api/secrets',
    });
  }

  async listNames() {
    const params = { teamId: this.teamId };
    return this.session.request({ method: 'get', path: '/list', params });
  }

  async fetchValue(name) {
    const params = { name, teamId: this.teamId };
    return this.session.request({ method: 'get', path: '', params });
  }

  async fetchValues(names) {
    const data = { names, teamId: this.teamId };
    return this.session.request({ method: 'post', path: '/batch', data });
  }

  async storeValue(name, value, { description } = {}) {
    const data = { name, value, description, teamId: this.teamId };
    return this.session.request({ method: 'post', path: '', data });
  }

  async removeValue(name) {
    const data = { name, teamId: this.teamId };
    return this.session.request({ method: 'post', path: '/remove', data });
  }
}

module.exports = { SecretsClient };
