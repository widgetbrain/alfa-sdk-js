const { BaseClient } = require('./common/base');

class CacheClient extends BaseClient {
  constructor(session) {
    super(session);

    this.session.setDefault({
      service: 'core',
      prefix: '/api/caches',
    });
  }

  async storeValue(key, value, ttl = 3600) {
    const data = { value, ttl };
    return this.session.request({ method: 'post', path: `/${key}`, data });
  }

  async fetchValue(key) {
    return this.session.request({ method: 'get', path: `/${key}` });
  }
}

module.exports = { CacheClient };
