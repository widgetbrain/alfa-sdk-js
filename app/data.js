const axios = require('axios');
const FormData = require('form-data');
const { BaseClient } = require('./common/base');

class DataClient extends BaseClient {
  constructor(session, { teamId } = {}) {
    super(session, { teamId });

    this.session.setDefault({
      service: 'data',
      prefix: '/api/storages',
    });
  }

  async fetchDataFile(dataFileId) {
    const url = await this.session.request({ method: 'get', path: `/download/${dataFileId}` });
    const res = await axios({ url: url.downloadLink, method: 'get', responseType: 'arraybuffer' });
    return res.data;
  }

  async listDataFiles({ prefix = '', skip = 0, limit = 100, order = 'name ASC' } = {}) {
    const params = { prefix, skip, limit, order };
    return this.session.request({ method: 'get', path: '/list', params });
  }

  async updateDataFile(dataFileId, changes) {
    const data = { updates: changes };
    return this.session.request({ method: 'put', path: `/update/${dataFileId}`, data });
  }

  async uploadDataFile(name, content, { teamId, description } = {}) {
    const form = new FormData();
    form.append('file', content, name);
    const params = { name };
    if (teamId) params.teamId = teamId;
    if (description) params.description = description;
    return this.session.request({
      method: 'post',
      path: '/upload',
      params,
      data: form,
      headers: form.getHeaders(),
    });
  }
}

module.exports = { DataClient };
