const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
chai.should();

const config = require('../config');
const { Authentication } = require('../../app/common/auth');
const { Session } = require('../../app/common/session');
const { BaseClient } = require('../../app/common/base');
const { AuthStore } = require('../../app/common/stores');
const { AuthenticationError, ResourceNotFoundError } = require('../../app/common/exceptions');

//

describe('session', () => {
  let creds;
  before(async () => {
    const params = await config.fetchParameters([config.clientId, config.clientSecret]);

    creds = {
      clientId: params[config.clientId],
      clientSecret: params[config.clientSecret],
    };

    AuthStore.purge();
  });

  describe('authentication', () => {
    let auth;
    before(async () => {
      auth = await Authentication.initialize(creds, 'dev');
    });

    it('should request token', () => {
      auth.should.have.property('token');
      auth.token.toLowerCase().startsWith('bearer').should.equal(true);
    });

    it('should authenticate creds (argument)', () => {
      auth.should.have.property('user');
      auth.user.should.have.property('username', 'wbtest');
      auth.user.should.have.property('userid');
      auth.user.should.have.property('teamId');
      auth.user.should.have.property('role');
    });

    it('should authenticate creds (environment)', async () => {
      process.env.ALFA_CLIENT_ID = creds.clientId;
      process.env.ALFA_CLIENT_SECRET = creds.clientSecret;
      const newAuth = await Authentication.initialize({}, 'dev');
      delete process.env.ALFA_CLIENT_ID;
      delete process.env.ALFA_CLIENT_SECRET;

      newAuth.should.have.property('token');
      newAuth.token.toLowerCase().startsWith('bearer').should.equal(true);
      newAuth.user.should.have.property('username', 'wbtest');
    });

    it('should not authenticate invalid creds', async () => {
      const promise = Authentication.initialize(creds, 'prod');
      await promise.should.be.rejectedWith(AuthenticationError);
    });

    context('token', () => {
      it('should authenticate token (argument)', async () => {
        const newAuth = await Authentication.initialize({ token: auth.token }, 'dev');

        newAuth.should.have.property('token');
        newAuth.token.toLowerCase().startsWith('bearer').should.equal(true);
        newAuth.user.should.have.property('username', 'wbtest');
      });

      it('should authenticate token (environment)', async () => {
        process.env.ALFA_TOKEN = auth.token;
        const newAuth = await Authentication.initialize({}, 'dev');
        delete process.env.ALFA_TOKEN;

        newAuth.should.have.property('token');
        newAuth.token.toLowerCase().startsWith('bearer').should.equal(true);
        newAuth.user.should.have.property('username', 'wbtest');
      });

      it('should authenticate token (environment - compatibility)', async () => {
        process.env.ALFA_AUTH0_TOKEN = auth.token;
        const newAuth = await Authentication.initialize({}, 'dev');
        delete process.env.ALFA_AUTH0_TOKEN;

        newAuth.should.have.property('token');
        newAuth.token.toLowerCase().startsWith('bearer').should.equal(true);
        newAuth.user.should.have.property('username', 'wbtest');
      });
    });
  });

  describe('session', () => {
    it('should assign default alfa env', async () => {
      const promise = Session.initialize(creds);
      await promise.should.be.rejectedWith(AuthenticationError);
    });

    it('should assign alfa env (arguments)', async () => {
      const session = await Session.initialize(creds, { alfaEnv: 'dev' });

      session.auth.should.have.property('token');
      session.auth.token.toLowerCase().startsWith('bearer').should.equal(true);
      session.auth.user.should.have.property('username', 'wbtest');
    });

    it('should assign alfa env (environment)', async () => {
      process.env.ALFA_ENV = 'dev';
      const session = await Session.initialize(creds);
      delete process.env.ALFA_ENV;

      session.auth.should.have.property('token');
      session.auth.token.toLowerCase().startsWith('bearer').should.equal(true);
      session.auth.user.should.have.property('username', 'wbtest');
    });
  });

  describe('base client', () => {
    let session;
    let client;
    before(async () => {
      session = await Session.initialize(creds, { alfaEnv: 'dev' });
      client = await BaseClient.initialize({ session });
    });

    it('should process request (basic)', async () => {
      await client.session.request({
        method: 'get',
        service: 'core',
        path: '/',
      });
    });

    it('should process request (params)', async () => {
      const params = { teamId: client.user.teamId };
      const team = await client.session.request({
        method: 'get',
        service: 'core',
        path: '/api/Teams/getInfo',
        params,
      });

      team.should.have.property('name', 'AI Algorithms');
    });

    it('should process request (default)', async () => {
      client.session.setDefault({ service: 'core', prefix: '/api' });
      const params = { teamId: client.user.teamId };
      const res = await client.session.request({
        method: 'get',
        path: '/Teams/getInfo',
        params,
        parse: false,
      });
      const team = res.data;

      client.session.setDefault({});
      res.config.should.have.property('url').that.includes('wb-core-development.widgetbrain');
      team.should.have.property('name', 'AI Algorithms');
    });

    it('should process request (default environment)', async () => {
      client.session.setDefault({ service: 'core', environment: 'production', prefix: '/api' });
      const params = { teamId: client.user.teamId };
      const res = await client.session.request({
        method: 'get',
        path: '/Teams/getInfo',
        params,
        parse: false,
      });

      client.session.setDefault({});
      res.config.should.have.property('url').that.includes('wb-core.widgetbrain');
    });

    it('should invoke algorithm', async () => {
      const algorithmId = 'bb1d784a-bc4d-4f8a-9805-25d8a38baa29';
      const environment = 'alfa-sdk';
      const problem = { value: 70 };

      const res = await client.session.invoke(algorithmId, environment, problem);
      res.should.have.property('out', 140);
    }).timeout(5000);

    it('should invoke integration', async () => {
      const integrationId = '9a882618-336e-4988-b85c-dc98b7e13c4f:alfa-sdk';
      const environment = 'multiply';
      const problem = { value: 70 };

      const res = await client.session.invoke(integrationId, environment, problem, {
        functionType: 'integration',
        functionName: 'double',
      });
      res.should.have.property('out', 140);
    }).timeout(5000);

    it('should throw error (no token)', async () => {
      const token = client.session.httpSession.defaults.headers['wb-authorization'];

      delete client.session.httpSession.defaults.headers['wb-authorization'];
      const res = await client.session.request({
        method: 'get',
        service: 'baas',
        path: '/',
        parse: false,
      });
      client.session.httpSession.defaults.headers['wb-authorization'] = token;

      const call = () => Session.parseResponse(res);
      chai.expect(call).to.throw(AuthenticationError);
    });

    it('should throw error (no resource)', async () => {
      const promise = client.session.request({
        method: 'get',
        service: 'baas',
        path: '/api/AlgorithmEnvironments/getInfo/XXX',
      });
      await promise.should.be.rejectedWith(ResourceNotFoundError);
    });

    context('context', () => {
      it('should process context (argument)', async () => {
        const newClient = await BaseClient.initialize({
          context: { token: session.auth.token, alfaEnvironment: 'dev' },
        });

        const params = { teamId: newClient.user.teamId };
        const team = await newClient.session.request({
          method: 'get',
          service: 'core',
          path: '/api/Teams/getInfo',
          params,
        });

        client.user.should.have.property('username', 'wbtest');
        team.should.have.property('name', 'AI Algorithms');
      });

      it('should process context (environment)', async () => {
        process.env.ALFA_TOKEN = session.auth.token;
        process.env.ALFA_ENV = 'dev';
        const newClient = await BaseClient.initialize();
        delete process.env.ALFA_TOKEN;
        delete process.env.ALFA_ENV;

        const params = { teamId: newClient.user.teamId };
        const team = await newClient.session.request({
          method: 'get',
          service: 'core',
          path: '/api/Teams/getInfo',
          params,
        });

        client.user.should.have.property('username', 'wbtest');
        team.should.have.property('name', 'AI Algorithms');
      });
    });
  });
});
