const chai = require('chai');
const fs = require('fs');
const { BaseStore } = require('../../app/common/stores');

chai.should();

describe('store', () => {
  const StubStore = new BaseStore('.this-is-a-test-store');

  after(() => {
    StubStore.purge();
  });

  it('should resolve file path', () => {
    const path = StubStore.getFilePath();
    path.should.contain(StubStore.FILE_NAME);
  });

  it('should initialize store', () => {
    const path = StubStore.getFilePath();
    const existsBefore = fs.existsSync(path);
    StubStore.setValue('initialized', 'true');
    const existsAfter = fs.existsSync(path);

    chai.expect(existsBefore).to.equal(false);
    chai.expect(existsAfter).to.equal(true);
  });

  it('should get value', () => {
    const value = StubStore.getValue('initialized');
    chai.expect(value).to.equal(true);
  });

  it('should get value from store object', () => {
    const store = StubStore.getStore();
    const value = store[StubStore.DEFAULT_SECTION].initialized;
    chai.expect(value).to.equal(true);
  });

  it('should get default value', () => {
    const value = StubStore.getValue('nonexistent_value', { default: 'default_value' });
    chai.expect(value).to.equal('default_value');
  });

  it('should set value - default section', () => {
    StubStore.setValue('new_key', 'new_value');
    const value = StubStore.getValue('new_key');
    chai.expect(value).to.equal('new_value');
  });

  it('should set value - custom section', () => {
    StubStore.setValue('new_key', 'another_new_value', { section: 'new.section' });
    const value = StubStore.getValue('new_key', { section: 'new.section' });
    chai.expect(value).to.equal('another_new_value');
  });

  it('should purge store', () => {
    const filePath = StubStore.getFilePath();
    StubStore.purge();
    const exists = fs.existsSync(filePath);
    chai.expect(exists).to.equal(false);
  });
});
