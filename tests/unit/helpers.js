const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
chai.should();

const { Session } = require('../../app/common/session');
const { EndpointHelper } = require('../../app/common/helpers');
const { UnknownServiceError, ServiceEnvironmentError } = require('../../app/common/exceptions');

//

describe('helpers', () => {
  describe('endpoint', () => {
    let endpoint;
    let endpointDev;

    before(() => {
      endpoint = new EndpointHelper('prod');
      endpointDev = new EndpointHelper('dev');
    });

    it('should initialize helper', () => {
      const endpointNew = Session.initializeEndpointHelper({ alfaEnv: 'test', alfaID: 'quinyx' });
      endpointNew.should.be.an.instanceOf(EndpointHelper);
      endpointNew.alfaID.should.equal('quinyx');
      endpointNew.alfaEnv.should.equal('test');
    });

    it('should resolve url', () => {
      const url = endpoint.resolve('core');
      url.should.equal('https://wb-core.widgetbrain.io');
    });

    it('should resolve url path', () => {
      const url = endpoint.resolve('core', '/api');
      url.should.equal('https://wb-core.widgetbrain.io/api');
    });

    it('should resolve url path - array', () => {
      const url = endpointDev.resolve('ais');
      url.should.equal('https://ais-dev.widgetbrain.io');
    });

    it('should resolve url path - specified environment', () => {
      const url = endpointDev.resolve('ais', '/', { environment: 'production' });
      url.should.equal('https://ais.widgetbrain.io/');
    });

    it('should not resolve invalid service', () => {
      const call = () => endpoint.resolve('core_invalid');
      chai.expect(call).to.throw(UnknownServiceError);
    });

    it('should not resolve invalid environment', () => {
      const invalid = new EndpointHelper('invalid_env');
      const call = () => invalid.resolve('core');
      chai.expect(call).to.throw(ServiceEnvironmentError);
    });

    it('should resolve a non-public ALFA url', () => {
      const endpointNonPublic = new EndpointHelper('dev', (alfaID = 'quinyx'));
      const url = endpointNonPublic.resolve('baas');
      url.should.equal('https://alfa-baas-eu-central-1.web-test.quinyx.com');
    });

    it('should resolve a non-public ALFA url in non-default region', () => {
      const endpointNonPublic = new EndpointHelper(
        'dev',
        (alfaID = 'quinyx'),
        (region = 'us-east-1'),
      );
      const url = endpointNonPublic.resolve('baas');
      url.should.equal('https://alfa-baas-us-east-1.web-test.quinyx.com');
    });
  });
});
