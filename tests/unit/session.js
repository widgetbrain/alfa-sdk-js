const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const nock = require('nock');
const { v4: uuid } = require('uuid');

chai.use(chaiAsPromised);
chai.should();

const { Session } = require('../../app/common/session');
const Auth = require('../../app/common/auth');
const { DEFAULT_SETTINGS } = require('../../app/common/data/config.json');
const { RequestError } = require('../../app/common/exceptions');
const { AuthStore } = require('../../app/common/stores');
const { EndpointHelper } = require('../../app/common/helpers');

//

describe('session', () => {
  describe('initialize', () => {
    beforeEach(() => {
      sinon.replace(Auth.Authentication, 'validateToken', sinon.fake.resolves({}));
      sinon.replace(
        Auth.Authentication,
        'requestOauthToken',
        sinon.fake.resolves('THE_REQUESTED_TOKEN'),
      );
    });

    afterEach(() => {
      sinon.restore();
      if ('ALFA_TOKEN' in process.env) delete process.env.ALFA_TOKEN;
      if ('ALFA_CONTEXT' in process.env) delete process.env.ALFA_CONTEXT;
      if ('ALFA_ID' in process.env) delete process.env.ALFA_ID;
      if ('ALFA_ENV' in process.env) delete process.env.ALFA_ENV;
      if ('PLATFORM_REGION' in process.env) delete process.env.PLATFORM_REGION;
    });

    context('authentication', () => {
      it('should prioritize clientId and clientSecret over an existing token', async () => {
        // ARRANGE
        process.env.ALFA_TOKEN = 'SOME_TOKEN';
        const credentials = { clientId: 'SOME_CLIENT_ID', clientSecret: 'SOME_CLIENT_SECRET' };

        // ACT
        await Session.initialize(credentials);

        // ARRANGE
        sinon.assert.calledOnce(Auth.Authentication.requestOauthToken);
        // eslint-disable-next-line no-unused-vars
        const [_, clientId, clientSecret] = Auth.Authentication.requestOauthToken.getCall(0).args;
        clientId.should.equal(credentials.clientId);
        clientSecret.should.equal(credentials.clientSecret);

        sinon.assert.calledOnce(Auth.Authentication.validateToken);
        const validatedToken = Auth.Authentication.validateToken.getCall(0).args[1];
        validatedToken.should.equal('THE_REQUESTED_TOKEN');
      });

      it('should use the existing token when no clientId or clientSecret are provided', async () => {
        // ARRANGE
        process.env.ALFA_TOKEN = 'SOME_TOKEN';
        const credentials = {};

        // ACT
        await Session.initialize(credentials);

        // ASSERT
        sinon.assert.notCalled(Auth.Authentication.requestOauthToken);
      });

      it('should append neo sessionid as cookie', async () => {
        // Arrange
        const credentials = { clientId: 'SOME_CLIENT_ID', clientSecret: 'SOME_CLIENT_SECRET' };
        const context = { neoSessionId: 'SESSIONID' };

        // Act
        const session = await Session.initialize(credentials, { context });
        const auth = session.auth;

        // Assert
        auth.should.have.property('cookie').that.contains('SESSIONID=SESSIONID');
        session.httpSession.defaults.headers.should.have
          .property('cookie')
          .that.contains('SESSIONID=SESSIONID');
      });

      it('should append neo sessionid to existing cookie', async () => {
        // Arrange
        const credentials = { cookie: 'test=test; SESSIONID=SOMETHING; XSESSIONID=XSESSIONID' };
        const context = { neoSessionId: 'SESSIONID' };

        // Act
        const session = await Session.initialize(credentials, { context });
        const auth = session.auth;

        // Assert
        auth.should.have.property('cookie').that.contains('SESSIONID=SESSIONID');
        session.httpSession.defaults.headers.should.have
          .property('cookie')
          .that.contains('SESSIONID=SESSIONID')
          .and.that.contains('XSESSIONID=XSESSIONID');
      });
    });

    context('configuration', () => {
      it('should prioritize the client-specified configuration over all other sources of configuration', async () => {
        // ARRANGE
        process.env.ALFA_ID = 'ENVVAR_ALFA_ID';
        process.env.PLATFORM_REGION = 'ENVVAR_PLATFORM_REGION';
        process.env.ALFA_ENV = 'ENVVAR_ALFA_ENV';
        process.env.ALFA_CONTEXT = JSON.stringify({
          alfaEnvironment: 'ENVVAR_CONTEXT_ALFA_ENV',
          alfaID: 'ENVVAR_CONTEXT_ALFA_ID',
          platformRegion: 'ENVVAR_CONTEXT_PLATFORM_REGION',
        });
        sinon.replace(AuthStore, 'getValue', sinon.fake.returns('CONFIG_STORE_VALUE'));

        const endPointHelperSetProperties = sinon.fake();
        sinon.replace(EndpointHelper.prototype, 'setProperties', endPointHelperSetProperties);
        sinon.replace(EndpointHelper.prototype, 'resolve', sinon.fake());

        const clientSpecifiedAlfaEnv = 'CLIENT_SPECIFIED_ALFA_ENV';
        const clientSpecifiedAlfaID = 'CLIENT_SPECIFIED_ALFA_ID';
        const clientSpecifiedPlatformRegion = 'CLIENT_SPECIFIED_PLATFORM_REGION';

        // ACT
        await Session.initialize(
          { clientId: 'SOME_CLIENT_ID', clientSecret: 'SOME_CLIENT_SECRET' },
          {
            alfaEnv: clientSpecifiedAlfaEnv,
            alfaID: clientSpecifiedAlfaID,
            platformRegion: clientSpecifiedPlatformRegion,
          },
        );

        // ASSERT
        endPointHelperSetProperties
          .alwaysCalledWith(
            clientSpecifiedAlfaEnv,
            clientSpecifiedAlfaID,
            clientSpecifiedPlatformRegion,
          )
          .should.equal(true);
      });

      it('should prioritize the environment variables configuration over the envvar context and config store', async () => {
        // ARRANGE
        process.env.ALFA_ID = 'ENVVAR_ALFA_ID';
        process.env.PLATFORM_REGION = 'ENVVAR_PLATFORM_REGION';
        process.env.ALFA_ENV = 'ENVVAR_ALFA_ENV';
        process.env.ALFA_CONTEXT = JSON.stringify({
          alfaEnvironment: 'ENVVAR_CONTEXT_ALFA_ENV',
          alfaID: 'ENVVAR_CONTEXT_ALFA_ID',
          platformRegion: 'ENVVAR_CONTEXT_PLATFORM_REGION',
        });
        sinon.replace(AuthStore, 'getValue', sinon.fake.returns('CONFIG_STORE_VALUE'));

        const endPointHelperSetProperties = sinon.fake();
        sinon.replace(EndpointHelper.prototype, 'setProperties', endPointHelperSetProperties);
        sinon.replace(EndpointHelper.prototype, 'resolve', sinon.fake());

        // ACT
        await Session.initialize({
          clientId: 'SOME_CLIENT_ID',
          clientSecret: 'SOME_CLIENT_SECRET',
        });

        // ASSERT
        endPointHelperSetProperties
          .alwaysCalledWith(process.env.ALFA_ENV, process.env.ALFA_ID, process.env.PLATFORM_REGION)
          .should.equal(true);
      });

      it('should prioritize the environment variable alfa context over the config store', async () => {
        // ARRANGE
        process.env.ALFA_CONTEXT = JSON.stringify({
          alfaEnvironment: 'ENVVAR_CONTEXT_ALFA_ENV',
          alfaID: 'ENVVAR_CONTEXT_ALFA_ID',
          platformRegion: 'ENVVAR_CONTEXT_PLATFORM_REGION',
        });
        sinon.replace(AuthStore, 'getValue', sinon.fake.returns('CONFIG_STORE_VALUE'));

        const endPointHelperSetProperties = sinon.fake();
        sinon.replace(EndpointHelper.prototype, 'setProperties', endPointHelperSetProperties);
        sinon.replace(EndpointHelper.prototype, 'resolve', sinon.fake());

        // ACT
        await Session.initialize({
          clientId: 'SOME_CLIENT_ID',
          clientSecret: 'SOME_CLIENT_SECRET',
        });

        // ASSERT
        const context = JSON.parse(process.env.ALFA_CONTEXT);
        endPointHelperSetProperties
          .alwaysCalledWith(context.alfaEnvironment, context.alfaID, context.platformRegion)
          .should.equal(true);
      });

      it('should use the config store when no other sources of configuration are available', async () => {
        // ARRANGE
        sinon.replace(AuthStore, 'getValue', sinon.fake.returns('CONFIG_STORE_VALUE'));

        const endPointHelperSetProperties = sinon.fake();
        sinon.replace(EndpointHelper.prototype, 'setProperties', endPointHelperSetProperties);
        sinon.replace(EndpointHelper.prototype, 'resolve', sinon.fake());

        // ACT
        await Session.initialize({
          clientId: 'SOME_CLIENT_ID',
          clientSecret: 'SOME_CLIENT_SECRET',
        });

        // ASSERT
        endPointHelperSetProperties
          .alwaysCalledWith('CONFIG_STORE_VALUE', 'CONFIG_STORE_VALUE', 'CONFIG_STORE_VALUE')
          .should.equal(true);
      });

      it('should fall back to the default values when no sources of configuration are available', async () => {
        // ARRANGE
        sinon.replace(AuthStore, 'getValue', sinon.fake());

        const endPointHelperSetProperties = sinon.fake();
        sinon.replace(EndpointHelper.prototype, 'setProperties', endPointHelperSetProperties);
        sinon.replace(EndpointHelper.prototype, 'resolve', sinon.fake());

        // ACT
        await Session.initialize({
          clientId: 'SOME_CLIENT_ID',
          clientSecret: 'SOME_CLIENT_SECRET',
        });

        // ASSERT
        endPointHelperSetProperties
          .alwaysCalledWith('prod', 'public', 'eu-central-1')
          .should.equal(true);
      });
    });
  });

  context('request', () => {
    beforeEach(() => {
      sinon.replace(Auth.Authentication, 'validateToken', sinon.fake());
      sinon.replace(Auth.Authentication, 'requestOauthToken', sinon.fake());
    });

    afterEach(() => {
      sinon.restore();
      process.env.ALFA_CONTEXT = undefined;
    });

    it('should resolve auth token', async () => {
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      const scope = nock('https://baas-development.widgetbrain.io')
        .get('/api/foo')
        .query(true)
        .reply(200, function response() {
          return this.req.options;
        });

      // ACT
      const request = await session.request({
        method: 'get',
        service: 'baas',
        path: '/api/foo',
        json: {},
      });

      // ASSERT
      scope.done();
      request.headers.should.have.property('wb-authorization', token);
      request.headers.should.not.have.property('cookie');
    });

    it('should resolve auth with neoSessionId', async () => {
      // ARRANGE
      const token = uuid();
      const session = await Session.initialize(
        { token },
        { alfaEnv: 'dev', context: { neoSessionId: 'SESSIONID' } },
      );

      const scope = nock('https://baas-development.widgetbrain.io')
        .get('/api/foo')
        .query(true)
        .reply(200, function response() {
          return this.req.options;
        });

      // ACT
      const request = await session.request({
        method: 'get',
        service: 'baas',
        path: '/api/foo',
        json: {},
      });
      scope.done();

      // ASSERT
      request.headers.should.have.property('wb-authorization', token);
      request.headers.should.have.property('cookie').that.contains('SESSIONID=SESSIONID');
    });

    it('should resolve auth token according to priority', async () => {
      const tokenDirect = uuid();
      const tokenContext = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token: tokenContext, alfaEnvironment: 'dev' });

      const session = await Session.initialize({ token: tokenDirect }, { alfaEnv: 'dev' });
      const scope = nock('https://baas-development.widgetbrain.io')
        .get('/api/foo')
        .query(true)
        .reply(200, function response() {
          return this.req.options;
        });

      // ACT
      const request = await session.request({
        method: 'get',
        service: 'baas',
        path: '/api/foo',
        json: {},
      });

      // ASSERT
      scope.done();
      request.headers.should.have.property('wb-authorization', tokenDirect);
      request.headers.should.not.have.property('cookie');
    });

    it('should resolve auth cookie', async () => {
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ cookie: token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      const scope = nock('https://baas-development.widgetbrain.io')
        .get('/api/foo')
        .query(true)
        .reply(200, function response() {
          return this.req.options;
        });

      // ACT
      const request = await session.request({
        method: 'get',
        service: 'baas',
        path: '/api/foo',
        json: {},
      });

      // ASSERT
      scope.done();
      request.headers.should.not.have.property('wb-authorization');
      request.headers.should.have.property('cookie', token);
    });

    it('should call authentication with non-public alfa arguments', async () => {
      const authenticationSpy = sinon.spy(Auth.Authentication, 'initialize');

      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({
        cookie: token,
        alfaID: 'quinyx',
        alfaEnvironment: 'dev',
      });

      await Session.initialize({}, { alfaEnv: 'dev' });

      sinon.assert.calledOnce(authenticationSpy);
      sinon.assert.calledWith(
        authenticationSpy,
        sinon.match.any,
        'dev',
        'quinyx',
        DEFAULT_SETTINGS.platformRegion,
      );
    });

    it('should include timeout in the request to the http session', async () => {
      // ARRANGE
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      sinon.replace(session.httpSession, 'request', sinon.fake.resolves({ config: {}, data: [] }));

      // ACT
      await session.request({
        method: 'get',
        service: 'baas',
        path: '/api/foo',
        json: {},
        timeout: 5000,
      });

      // ASSERT
      session.httpSession.request
        .calledOnceWith({
          method: 'get',
          json: {},
          timeout: 5000,
          url: 'https://baas-development.widgetbrain.io/api/foo',
        })
        .should.equal(true);
    });
  });

  context('invoke', () => {
    beforeEach(() => {
      sinon.replace(Auth.Authentication, 'validateToken', sinon.fake());
      sinon.replace(Auth.Authentication, 'requestOauthToken', sinon.fake());
    });

    afterEach(() => {
      sinon.restore();
      process.env.ALFA_CONTEXT = undefined;
    });

    it('should not include the request options in the http session when none are defined', async () => {
      // ARRANGE
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      sinon.replace(session.httpSession, 'request', sinon.fake.resolves({ config: {}, data: [] }));

      // ACT
      await session.invoke('foo', 'bar', { lorem: 'ipsum' });

      // ASSERT
      session.httpSession.request.args.should.deep.equal([
        [
          {
            method: 'post',
            data: {
              algorithmId: 'foo',
              environment: 'bar',
              problem: { lorem: 'ipsum' },
              canBuffer: undefined,
              includeDetails: undefined,
              returnHoldingResponse: undefined,
              returnReference: undefined,
              dryRunOptions: {
                useExternalLogging: undefined,
                usePostProcessingIntegration: undefined,
                usePreProcessingIntegration: undefined,
              },
            },
            url: 'https://baas-development.widgetbrain.io/api/Algorithms/submitRequest',
          },
        ],
      ]);
    });

    it('should include the request options in the request to the http session', async () => {
      // ARRANGE
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      sinon.replace(session.httpSession, 'request', sinon.fake.resolves({ config: {}, data: [] }));

      // ACT
      await session.invoke(
        'foo',
        'bar',
        { lorem: 'ipsum' },
        { timeout: 2000, useExternalLogging: true, usePostProcessingIntegration: false },
      );

      // ASSERT
      session.httpSession.request.args.should.deep.equal([
        [
          {
            timeout: 2000,
            method: 'post',
            data: {
              algorithmId: 'foo',
              environment: 'bar',
              problem: { lorem: 'ipsum' },
              canBuffer: undefined,
              includeDetails: undefined,
              returnHoldingResponse: undefined,
              returnReference: undefined,
              dryRunOptions: {
                useExternalLogging: true,
                usePostProcessingIntegration: false,
                usePreProcessingIntegration: undefined,
              },
            },
            url: 'https://baas-development.widgetbrain.io/api/Algorithms/submitRequest',
          },
        ],
      ]);
    });
  });

  context('parseResponse', () => {
    it('should throw a request error when an error message is not a string', () => {
      // ARRANGE
      const res = {
        config: { url: 'http://foo.bar/baz' },
        data: {
          error: {
            message: [
              {
                field: 'requests[0].forecastDataPayload[37].data',
                message: 'error.forecast.requests.forecastDataPayLoad.data.empty',
                additionalErrorParameters: {},
              },
              {
                field: 'requests[0].forecastDataPayload[58].data',
                message: 'error.forecast.requests.forecastDataPayLoad.data.empty',
                additionalErrorParameters: {},
              },
            ],
          },
        },
      };

      // ACT & ASSERT
      (() => Session.parseResponse(res)).should.throw(RequestError);
    });
  });

  context('parseError', () => {
    beforeEach(() => {
      sinon.replace(Auth.Authentication, 'validateToken', sinon.fake());
      sinon.replace(Auth.Authentication, 'requestOauthToken', sinon.fake());
    });

    afterEach(() => {
      sinon.restore();
      nock.restore();
      process.env.ALFA_CONTEXT = undefined;
    });

    it('should return a RequestError', async () => {
      // ARRANGE
      const token = uuid();
      process.env.ALFA_CONTEXT = JSON.stringify({ token, alfaEnvironment: 'dev' });

      const session = await Session.initialize({}, { alfaEnv: 'dev' });
      nock('https://baas-development.widgetbrain.io')
        .get('/api/foo')
        .query(true)
        .delay(1000)
        .reply(200, function response() {
          return this.req.options;
        });

      let error;
      try {
        await session.request({
          method: 'get',
          service: 'baas',
          path: '/api/foo',
          json: {},
          timeout: 100,
        });
      } catch (e) {
        error = e;
      }

      // ACT
      const parsedError = Session.parseError(error);

      // ASSERT
      parsedError.should.be.instanceOf(RequestError);
      parsedError.code.should.equal('REQUEST_FAILED');
      parsedError.status.should.equal(408);
      parsedError.message.should.equal(
        'Error encountered during request to https://baas-development.widgetbrain.io/api/foo (408): ECONNABORTED: timeout of 100ms exceeded',
      );
    });
  });
});
