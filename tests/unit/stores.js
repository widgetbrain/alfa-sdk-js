const should = require('chai').should();
const path = require('path');
const tempy = require('tempy');
const { v4: uuid } = require('uuid');
const { BaseStore } = require('../../app/common/stores');

describe('stores', () => {
  describe('BaseStore', () => {
    describe('getValue', () => {
      context('when the store does not exist', () => {
        it('should return undefined', async () => {
          // ARRANGE
          const store = new BaseStore(path.resolve(tempy.root, uuid()));
          const key = 'A_RANDOM_KEY';
          const section = 'A_RANDOM_SECTION';

          // ACT
          const value = store.getValue(key, { section });

          // ASSERT
          should.not.exist(value);
        });
      });

      context('when the section does not exist', () => {
        it('should return undefined', async () => {
          // ARRANGE
          const store = new BaseStore(path.resolve(tempy.root, uuid()));
          store.setValue('SOME_KEY', 'SOME_VALUE', { section: 'SOME_SECTION' });

          const key = 'A_NON_EXISTENT_KEY';
          const section = 'A_NON_EXISTENT_SECTION';

          // ACT
          const value = store.getValue(key, { section });

          // ASSERT
          should.not.exist(value);
        });
      });

      context('when the key does not exist in an existing section', () => {
        it('should return undefined', async () => {
          // ARRANGE
          const store = new BaseStore(path.resolve(tempy.root, uuid()));
          store.setValue('SOME_KEY', 'SOME_VALUE', { section: 'AN_EXISTING_SECTION' });

          const key = 'A_NON_EXISTENT_KEY';
          const section = 'AN_EXISTING_SECTION';

          // ACT
          const value = store.getValue(key, { section });

          // ASSERT
          should.not.exist(value);
        });
      });

      context('when the key exists in an existing section', () => {
        it('should return the value of the key', async () => {
          // ARRANGE
          const store = new BaseStore(path.resolve(tempy.root, uuid()));
          store.setValue('AN_EXISTING_KEY', 'THE_STORED_VALUE', { section: 'AN_EXISTING_SECTION' });

          const key = 'AN_EXISTING_KEY';
          const section = 'AN_EXISTING_SECTION';

          // ACT
          const value = store.getValue(key, { section });

          // ASSERT
          value.should.equal('THE_STORED_VALUE');
        });
      });
    });
  });
});
