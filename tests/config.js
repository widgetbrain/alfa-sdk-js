module.exports = {
  clientId: '/alfa/development/user/1683/auth0/clientId',
  clientSecret: '/alfa/development/user/1683/auth0/clientSecret',
  clientIdMeta: '/alfa/development/user/1915/auth0/clientId',
  clientSecretMeta: '/alfa/development/user/1915/auth0/clientSecret',
  fetchParameters,
};

//

const WBParameterHandler = require('WBParameterHandler');

function fetchParameters(testParams) {
  return new Promise((resolve, reject) => {
    WBParameterHandler.loadParametersLambda({}, testParams, (err, res) => {
      if (err !== null) reject(err);
      else resolve(res);
    });
  });
}
