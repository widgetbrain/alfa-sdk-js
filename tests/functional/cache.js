const chai = require('chai');
const config = require('../config');
const { Session, CacheClient } = require('../../app');

chai.should();
chai.use(require('chai-as-promised'));

describe('cache :', () => {
  let cache;
  before(async () => {
    const params = await config.fetchParameters([config.clientId, config.clientSecret]);
    const credentials = {
      clientId: params[config.clientId],
      clientSecret: params[config.clientSecret],
    };

    const session = await Session.initialize(credentials, { alfaEnv: 'dev' });
    cache = new CacheClient(session);
  });

  context('cache storage', () => {
    const key = 'test-alfa-sdk-js';
    const value = 'test-value';

    it('should store a cache value', async () => {
      // ACT
      const res = await cache.storeValue(key, value);

      // ASSERT
      res.should.have.property('key').that.contains(`:${key}`);
      res.should.have.property('value', value);
      res.should.have.property('ttl');
    });

    it('should fetch a cache value', async () => {
      // ACT
      const res = await cache.fetchValue(key);

      // ASSERT
      res.should.have.property('key').that.contains(`:${key}`);
      res.should.have.property('value', value);
    });
  });
});
