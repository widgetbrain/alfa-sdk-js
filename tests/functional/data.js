const chai = require('chai');
const config = require('../config');
const { Session, DataClient } = require('../../app');

chai.should();
chai.use(require('chai-things'));
chai.use(require('chai-string'));

describe('data :', () => {
  let data;

  before(async () => {
    const params = await config.fetchParameters([config.clientId, config.clientSecret]);
    const credentials = {
      clientId: params[config.clientId],
      clientSecret: params[config.clientSecret],
    };

    const session = await Session.initialize(credentials, { alfaEnv: 'dev' });
    data = new DataClient(session);
  });

  context('list data files', () => {
    it('should return an array with the files of the team', async () => {
      // ACT
      const res = await data.listDataFiles();

      // ASSERT
      res.should.be.an('array');
      res.should.all.have.property('name');
      res.should.all.have.property('description');
      res.should.all.have.property('id');
      res.should.all.have.property('teamId', data.teamId);
      res.should.include.something.that.has.property('name', 'alfa-sdk.json');
    });

    it('should return an array with the files filtered by the prefix', async () => {
      // ARRANGE
      const prefix = 'alfa-sdk';

      // ACT
      const res = await data.listDataFiles({ prefix });

      // ASSERT
      res.should.be.an('array');
      res.should.satisfy((files) =>
        files.every((file) => file.should.have.property('name').that.startWith(prefix)),
      );
    });
  });

  context('fetch data file', () => {
    it('should return a Buffer containing the data of the file', async () => {
      // ARRANGE
      const files = await data.listDataFiles({ prefix: 'alfa-sdk.json' });
      const dataFileId = files[0].id;

      // ACT
      const res = await data.fetchDataFile(dataFileId);

      // ASSERT
      res.should.be.instanceof(Buffer);
      const content = JSON.parse(res.toString('utf8'));
      content.should.be.an('object').and.have.property('ModerationLabels');
    });
  });

  context('upload data file', () => {
    afterEach(async () => {
      const files = await data.listDataFiles({ prefix: 'alfa-sdk-' });
      const fileNamesToBeDeleted = ['alfa-sdk-string', 'alfa-sdk-buffer'];

      for (const file of files) {
        if (fileNamesToBeDeleted.includes(file.name)) {
          await data.session.request({ method: 'delete', path: `/deletefile/${file.id}` });
        }
      }
    });

    it("should upload the string to the team's data storage", async () => {
      // ARRANGE
      const start = new Date().getTime();
      const content = 'hello, world!';
      const name = 'alfa-sdk-string';

      // ACT
      const res = await data.uploadDataFile(name, content);

      // ASSERT
      res.should.be.an('object');
      res.should.have.property('id');
      res.should.have.property('name', name);
      res.should.have
        .property('uploadTime')
        .that.satisfy((timestamp) => new Date(timestamp).getTime() >= start);
      res.should.have.property('teamId', data.teamId);

      const fileContent = await data.fetchDataFile(res.id);
      fileContent.toString('utf8').should.equal(content);
    });

    it("should upload the Buffer to the team's data storage", async () => {
      // ARRANGE
      const start = new Date().getTime();
      const content = Buffer.from('lorem ipsum');
      const name = 'alfa-sdk-buffer';

      // ACT
      const res = await data.uploadDataFile(name, content);

      // ASSERT
      res.should.be.an('object');
      res.should.have.property('id');
      res.should.have.property('name', name);
      res.should.have
        .property('uploadTime')
        .that.satisfy((timestamp) => new Date(timestamp).getTime() >= start);
      res.should.have.property('teamId', data.teamId);

      const fileContent = await data.fetchDataFile(res.id);
      fileContent.toString('utf8').should.equal(content.toString('utf8'));
    });
  });

  context('update data file', () => {
    afterEach(async () => {
      const files = await data.listDataFiles({ prefix: 'alfa-sdk-update' });

      for (const file of files) {
        await data.session.request({ method: 'delete', path: `/deletefile/${file.id}` });
      }
    });

    it('should apply the changes to the data file', async () => {
      // ARRANGE
      const content = 'hello, world!';
      const name = 'alfa-sdk-update';
      const description = 'original description';
      const { id } = await data.uploadDataFile(name, content, { description });

      const [createdFile] = await data.listDataFiles({ prefix: name });
      createdFile.should.have.property('id', id);
      createdFile.should.have.property('description', description);

      const changes = { description: 'updated description' };

      // ACT
      await data.updateDataFile(id, changes);

      const [updatedFile] = await data.listDataFiles({ prefix: name });
      updatedFile.should.have.property('id', id);
      updatedFile.should.have.property('description', changes.description);
    });
  });
});
