const chai = require('chai');
const { sleep } = require('wb-node-utils/lib/time');
const config = require('../config');
const { Session, SecretsClient } = require('../../app');

chai.should();
chai.use(require('chai-things'));

describe('secrets :', () => {
  let secrets;
  before(async () => {
    const params = await config.fetchParameters([config.clientId, config.clientSecret]);
    const credentials = {
      clientId: params[config.clientId],
      clientSecret: params[config.clientSecret],
    };

    const session = await Session.initialize(credentials, { alfaEnv: 'dev' });
    secrets = new SecretsClient(session);
  });

  context('list secrets', () => {
    it('should return an array with the secrets of the team', async () => {
      // ACT
      const res = await secrets.listNames();

      // ASSERT
      res.should.be.an('array');
      res.should.all.have.property('name');
      res.should.all.have.property('description');
      res.should.all.have.property('id');
      res.should.all.have.property('teamId', secrets.teamId);
    });
  });

  context('fetch a secret', () => {
    const name = 'alfasdkjstest';
    const value = 'ALFA_SDK_JS_TEST';

    before(async () => {
      await secrets.storeValue(name, value);
    });

    it('should return the value of the secret', async () => {
      // ACT
      const res = await secrets.fetchValue(name);

      // ASSERT
      res.should.equal(value);
    });

    after(async () => {
      await secrets.removeValue(name);
    });
  });

  context('fetch multiple secrets', () => {
    const secret1 = { name: 'alfasdkjstest1', value: 'ALFA_SDK_JS_TEST_1' };
    const secret2 = { name: 'alfasdkjstest2', value: 'ALFA_SDK_JS_TEST_2' };

    before(async () => {
      await secrets.storeValue(secret1.name, secret1.value);
      await secrets.storeValue(secret2.name, secret2.value);
    });

    it('should return the values of the secrets', async () => {
      // ACT
      const res = await secrets.fetchValues([secret1.name, secret2.name]);

      //  ASSERT
      res.should.deep.equal({
        alfasdkjstest1: 'ALFA_SDK_JS_TEST_1',
        alfasdkjstest2: 'ALFA_SDK_JS_TEST_2',
      });
    });

    after(async () => {
      await secrets.removeValue(secret1.name);
      await secrets.removeValue(secret2.name);
    });
  });

  context('store a secret', () => {
    const name = 'alfasdkjsstoretest';
    const value = 'ALFA_SDK_JS_TEST';

    before(async () => {
      const res = await secrets.fetchValue(name);
      res.should.equal('');
    });

    it('should store the secret correctly', async () => {
      // ACT
      await secrets.storeValue(name, value);

      // ASSERT
      const res = await secrets.fetchValue(name);
      res.should.equal(value);
    });

    after(async () => {
      await secrets.removeValue(name);
    });
  });

  context('remove a secret', () => {
    const name = 'alfasdkjsremovetest';
    const value = 'ALFA_SDK_JS_REMOVE_TEST';

    before(async () => {
      await secrets.storeValue(name, value);
      const res = await secrets.fetchValue(name);
      res.should.equal(value);
    });

    it('should remove the secret', async () => {
      // ACT
      await secrets.removeValue(name);

      // ASSERT
      await sleep('500ms'); // required because otherwise the value is not removed
      const res = await secrets.fetchValue(name);
      res.should.equal('');
    });
  });
});
